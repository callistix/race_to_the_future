# Race To The Future

## Overview

Small racing game made in two days for "Hack To The Future 2018" at King.

## Description

Drive your car to the future!
Collect radioactive fuel to charge the flux capacitor before the time is up!

## Installation

Download and install the free game engine [Löve2D](https://love2d.org/).
Run `love code/` from inside the game repository.

The game has been tested with Löve2D version 11 on Linux.

## Usage

Use the keyboard to steer the car, use up/down to accelerate, and left or right to turn.  
Try to collect the fuel buckets before the time is up.

There's a not-so-hidden key to cheat around the time limitation and fill the tank immediately, can you find it?

## Author and Contact

To report any issues or for suggestions contact me here or drop a mail to "vitaminx /at/ callistix.net".

## License

This software is released under the GNU General Public License Version 3.  
Please see the file COPYING for a copy of the license.

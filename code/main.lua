game = {}

require "lua/color"
require "lua/draw"
require "lua/update"
require "lua/const"
require "lua/load"

game.init = true
game.ft = 0
game.scale = 2
game.level = 1
game.distance = 0
game.mode = 0
game.trvmode = {}
game.black = 0
game.flux = 0
game.flux_max = 40
game.timer = 40

credits = {}
credits.t = 0
credits.t_max = 184
credits.mode = 1
credits.x_max = 200
credits.x = 0
credits.x_v = 20

function love.load()

    win = {}

    game.load_win()
    game.load_sprites()
    game.load_canvas()
    game.load_fonts()
    game.load_sounds()
    game.load_trees()
    game.load_gas()

    -- game vars
    player = {}
    player.pos = {}
    player.pos.x = 0
    player.pos.y = math.floor(win.h * prefs.car_y / 100)
    player.vec = {}
    player.vec.x = 0
    player.vec.y = 0
    player.v = 0
    player.s = 1
    player.pos.x_prev = 0
    player.pos.x_travel = 0

    -- set run flag
    game.mode = 1
end

function love.update(dt)
    if game.init then
        game.init = false
        return
    end

    game.ft = game.ft + dt

    if game.mode == 1 then
        game.update_timer(dt)
    end

    if game.ft < prefs.ft then
        return
    end

    game.ft = game.ft - prefs.ft

    game.update()
end

function love.draw()
    -- draw to canvas
    love.graphics.setCanvas(canvas)

    if game.mode ~= 4 then
        game.draw_bg()
    else
        love.graphics.setBackgroundColor(color[1])
        love.graphics.clear()
    end

    game.draw()

    -- blit to screen
    love.graphics.setColor(1, 1, 1, 1)
    love.graphics.setCanvas()
    love.graphics.draw(canvas, 0, 0, 0, game.scale, game.scale)
end

function love.keypressed(key)
    if key == "escape" then
        love.event.quit()
    elseif key == "kp+" then
        game.scale = game.scale + 1
        game.load_win()
    elseif key == "kp-" then
        game.scale = game.scale - 1
        if game.scale < 1 then
            game.scale = 1
        end
        game.load_win()
    elseif key == "s" then
        game.travel_init()
    end
end

function game.travel_init()
    game.mode = 3
    game.trvmode.t = 0
end

function game.travel()
    game.mode = 2
    game.trvmode.t = 0
    sfx.car.buf:setPitch(1)
end

function game.credits()
    game.mode = 4
    sfx.car.buf:setVolume(0)
    music.credits.buf:play()
end

function game.over()
    game.mode = 5
end

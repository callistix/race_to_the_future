function game.draw()
    if game.mode == 1 or game.mode == 3 or game.mode == 5 then
        game.draw_skyline()
        game.draw_road()
        game.draw_car()
        game.draw_hud()
    elseif game.mode == 2 then
        game.draw_travel_bg()
        game.draw_travel_road()
        game.draw_travel_car()
        game.draw_black()
    elseif game.mode == 4 then
        game.draw_credits()
        game.draw_black()
    end
end

function game.draw_bg()
    -- clear background
    love.graphics.setBackgroundColor(color[level[game.level].color.hi])
    love.graphics.clear()

    -- draw lower part of screen
    love.graphics.setColor(color[level[game.level].color.lo])
    love.graphics.rectangle("fill", 0, win.split, win.w, win.h - win.split)

    game.draw_sky_gradient()
end

function game.draw_hud()
    love.graphics.setFont(font.hud)
    love.graphics.setColor(color[10])
    love.graphics.print(level[game.level].time, prefs.hud.margin.x, prefs.hud.margin.y)

    -- draw flux level
    local flux_pct = 100 / (game.flux_max / game.flux)

    if flux_pct < 33 then
        col = 28
    elseif flux_pct < 66 then
        col = 6
    else
        col = 11
    end

    love.graphics.setColor(color[col])
    love.graphics.rectangle("fill", win.w - prefs.hud.flux.w - prefs.hud.margin.x, prefs.hud.margin.y, flux_pct, prefs.hud.flux.h )

    -- draw fluxbox frame
    love.graphics.setColor(color[22])
    love.graphics.rectangle("line", win.w - prefs.hud.flux.w - prefs.hud.margin.x, prefs.hud.margin.y, prefs.hud.flux.w, prefs.hud.flux.h )

    -- draw timer
    love.graphics.setFont(font.timer)
    love.graphics.setColor(color[9])
    love.graphics.print(string.format("%02d",math.floor(game.timer)), 144, 20)
end

function game.draw_car()
    love.graphics.setColor(color[22])

    local car_buf = 1

    if player.vec.y == -1 then
        car_buf = 2
    end

    love.graphics.draw(img.car.buf[car_buf], math.floor(win.w / 2 - img.car.w / 2), player.pos.y)
end

function game.draw_sky_gradient()
    local grad_y = win.split -  math.floor(win.h * prefs.sky_grad) / 100
    local grad_h = math.floor(win.h * prefs.sky_grad) / 100
    local grad_n = #level[game.level].color.sky_grad

    for i, c in ipairs(level[game.level].color.sky_grad) do
        love.graphics.setColor(color[c])
        love.graphics.rectangle("fill", 0, grad_y + (grad_h / grad_n) * (i - 1), win.w, grad_h / grad_n)
    end
end

function game.draw_road()
    local road_w = math.floor((win.w / 100) * level[game.level].road.w)
    local road_h = win.h - win.split + 1
    local road_x1 = (win.w - road_w) / 2
    local road_x2 = road_x1 + road_w
 
    for y = 1, road_h do
        local road_red = (road_w / 100) * (level[game.level].road.red / 2)
        local road_red = road_red * (1 - (y / road_h))

        -- calculate road gaps
        local dist = game.distance * 4 + (road_h - y * ((road_red + 300) / 200))

        if dist % level[game.level].road.gap > (level[game.level].road.gap / 2) then
            col = 1
        else
            col = 2
        end

        -- calculate perspective
        local psp = -((y/road_h) * player.pos.x) * prefs.psp

        love.graphics.setColor(color[level[game.level].color.road[col]])
        love.graphics.line(road_x1 + psp + road_red - player.pos.x, win.split + y, road_x2 + psp - road_red - player.pos.x, win.split + y)

        for i, obj in ipairs(gas) do
            if obj.draw then
                gas[i].y = ((win.split + (game.distance - obj.dist) ) * 1.6) + (gas[i].y / 2)
                gas[i].x = -player.pos.x * ((gas[i].y - 120) / 15)
                gas[i].x = gas[i].x + obj.h * ((gas[i].y - 120) / 2)
                gas[i].scale = (gas[i].y - 100) / 80
            end
        end

        for i, obj in ipairs(trees) do
            if obj.draw then
                trees[i].y = ((win.split + (game.distance - obj.dist) ) * 0.5) + (trees[i].y / 2)
                trees[i].x = -player.pos.x * ((trees[i].y - 120) / 15)
                trees[i].x = trees[i].x + obj.h * ((trees[i].y - 120) / 4)
                trees[i].scale = (trees[i].y - 100) / 80
            end
        end
    end

    love.graphics.setColor(color[22])
    -- draw gas
    for i, obj in ipairs(gas) do
        if obj.draw then
            love.graphics.draw(img.gas.buf, (win.w / 2) + obj.x - (img.gas.w / 2), obj.y, 0, obj.scale, obj.scale)
        end
    end

    -- draw trees
--    for i, obj in ipairs(trees) do
--        if obj.draw then
--            love.graphics.draw(img.tree.buf, img.tree.quad[1], (win.w / 2) + obj.x - ((img.tree.w / 5) / 2), obj.y, 0, obj.scale, obj.scale)
--        end
--    end
end

function game.draw_skyline()
    love.graphics.setColor(color[22])
    love.graphics.draw(level[game.level].bg.buf, -player.pos.x - 70, win.split - level[game.level].bg.h)
end

function game.draw_travel_road()
    local road_w = math.floor((win.w / 100) * level[game.level].road.w)
    local road_h = win.h - win.split + 1
    local road_x1 = (win.w - road_w) / 2
    local road_x2 = road_x1 + road_w
 
    for y = 1, road_h do
        local road_red = (road_w / 100) * (level[game.level].road.red / 2)
        local road_red = road_red * (1 - (y / road_h))

        -- calculate road gaps
        local dist = game.distance * 4 + (road_h - y * ((road_red + 300) / 200))

        if dist % level[game.level].road.gap > (level[game.level].road.gap / 2) then
            col = 1
        else
            col = 2
        end

        -- calculate perspective
        local psp = -((y/road_h) * player.pos.x_prev) * prefs.psp

        love.graphics.setColor(color[level[game.level].color.road[col]])
        love.graphics.line(road_x1 + psp + road_red - player.pos.x_prev, win.split + y, road_x2 + psp - road_red - player.pos.x_prev, win.split + y)
    end
end

function game.draw_travel_car()
    love.graphics.setColor(color[22])

    if game.mode == 2 then
        car_buf = 3
    else
        car_buf = 1
    end

    love.graphics.draw(img.car.buf[car_buf], math.floor(win.w / 2 - img.car.w / 2) + player.pos.x_travel, player.pos.y, 0, player.s, player.s)
end

function game.draw_travel_bg()
    love.graphics.setColor(color[22])
    love.graphics.draw(level[game.level].bg.buf, -player.pos.x_prev - 70, win.split - level[game.level].bg.h)
end

function game.draw_black()
    love.graphics.setColor(0, 0, 0, game.black)
    love.graphics.rectangle("fill", 0, 0, win.w, win.h)
end

function game.draw_credits()
    love.graphics.setFont(font.credits)
    love.graphics.setColor(color[10])
    if credits.mode == 1 then
        love.graphics.print("HACK TO THE FUTURE 2018", 110 + credits.x, 210)
    else
        love.graphics.print("ROMAN SCHMIDT", 188 + credits.x, 210)
    end
end

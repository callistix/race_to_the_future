prefs = {}
prefs.ft = 1/60
prefs.split = 60    -- vertical percentage of sky
prefs.sky_grad = 6  -- vertical percentage of sky gradient
prefs.psp = 7       -- strength of perspective distortion when turning
prefs.car_y = 78    -- percentage of default vertical car position
prefs.can_w = 320
prefs.can_h = 240
prefs.hud = {}
prefs.hud.font_size = 14
prefs.hud.margin = {}
prefs.hud.margin.x = 10
prefs.hud.margin.y = 10
prefs.hud.flux = {}
prefs.hud.flux.w = 100
prefs.hud.flux.h = 12
prefs.player = {}
prefs.player.v_max = 200
prefs.player.acc = 1
prefs.player.dec = 0.1
prefs.player.brake = 2
prefs.player.v_turn = 10
prefs.player.h_max = 155
prefs.sfx = {}
prefs.sfx.car_pitch = 0.01
prefs.lod = 5

level = {}
level[1] = {}
level[1].color = {}
level[1].color.hi = 18
level[1].color.lo = 12
level[1].color.sky_grad = {19, 20, 21}
level[1].color.road = {24, 25}
level[1].road = {}
level[1].road.w = 140   -- width of road at lower end of screen
level[1].road.red = 90  -- percentage of max. road width reduction
level[1].road.gap = 50  -- size of vertical subdivisions
level[1].time = "JUL 03 1985"

gas = {
    -- wave 1
    { dist =  200,  h = 0.0 },
    { dist =  220,  h = 0.4 },
    { dist =  240,  h = 0.8 },
    { dist =  260,  h = 1.2 },
    { dist =  280,  h = 1.6 },
    { dist =  300,  h = 2.0 },
    { dist =  320,  h = 2.4 },
    { dist =  340,  h = 2.8 },
    { dist =  360,  h = 2.8 },
    { dist =  380,  h = 2.4 },
    { dist =  400,  h = 2.0 },
    { dist =  420,  h = 1.6 },
    { dist =  440,  h = 1.2 },
    { dist =  460,  h = 0.8 },
    { dist =  480,  h = 0.4 },
    { dist =  500,  h = 0.0 },

    -- wave 2
    { dist = 1000,  h =  0.0 },
    { dist = 1020,  h = -0.4 },
    { dist = 1040,  h = -0.8 },
    { dist = 1060,  h = -1.2 },
    { dist = 1080,  h = -1.6 },
    { dist = 1100,  h = -2.0 },
    { dist = 1120,  h = -2.4 },
    { dist = 1140,  h = -2.8 },
    { dist = 1160,  h = -2.8 },
    { dist = 1180,  h = -2.4 },
    { dist = 1200,  h = -2.0 },
    { dist = 1220,  h = -1.6 },
    { dist = 1240,  h = -1.2 },
    { dist = 1260,  h = -0.8 },
    { dist = 1280,  h = -0.4 },
    { dist = 1300,  h = -0.0 },

    -- wave 3
    { dist = 1500,  h = -2.0 },
    { dist = 1550,  h = -2.0 },
    { dist = 1600,  h = -2.0 },
    { dist = 1650,  h = -2.0 },
    { dist = 1700,  h = -2.0 },

    { dist = 1900,  h =  2.0 },
    { dist = 1950,  h =  2.0 },
    { dist = 2000,  h =  2.0 },
    { dist = 2050,  h =  2.0 },
    { dist = 2100,  h =  2.0 },

    { dist = 2300,  h =  0.0 },
    { dist = 2350,  h =  0.0 },
    { dist = 2400,  h =  0.0 },
    { dist = 2450,  h =  0.0 },
    { dist = 2500,  h =  0.0 },

    -- wave 4
    { dist = 3000,  h =  0.0 },
    { dist = 3020,  h =  0.0 },
    { dist = 3080,  h =  3.0 },
    { dist = 3100,  h =  3.0 },
    { dist = 3160,  h = -3.0 },
    { dist = 3180,  h = -3.0 },
    { dist = 3240,  h =  0.0 },
    { dist = 3260,  h =  0.0 },
    { dist = 3320,  h =  3.0 },
    { dist = 3340,  h =  3.0 },
    { dist = 3400,  h = -3.0 },
    { dist = 3420,  h = -3.0 },
    { dist = 3480,  h =  0.0 },
    { dist = 3500,  h =  0.0 },
}

trees = {
    { dist =  110,  h = -14.0 },
    { dist =  120,  h =  14.2 },
    { dist =  122,  h = -17.7 },
    { dist =  127,  h =  17.9 },
    { dist =  131,  h = -14.0 },
}

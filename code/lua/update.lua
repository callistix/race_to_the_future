function game.update()
    if game.mode == 1 or game.mode == 5 then
        game.update_input()
        game.update_speed()
        game.update_pos()
        game.update_gas()
    elseif game.mode == 2 then
        game.update_travel()
    elseif game.mode == 3 then
        game.update_travel_prep()
        game.update_travel_input()
        game.update_speed()
        game.update_pos()
    end
    if game.mode == 4 then
        game.update_credits()
    end
end

function game.update_input()
    player.vec.x = 0
    player.vec.y = 0

    if love.keyboard.isDown("left") and not love.keyboard.isDown("right") then
        player.vec.x = -1
    elseif love.keyboard.isDown("right") and not love.keyboard.isDown("left") then
        player.vec.x = 1
    end

    if love.keyboard.isDown("down") then
        player.vec.y = -1
    elseif love.keyboard.isDown("up") then
        player.vec.y = 1
    end

    if game.mode == 5 then
        player.vec.x = 0
        player.vec.y = -1
    end
end

function game.update_travel_input()
    player.vec.x = 0
    player.vec.y = -1
end

function game.update_speed()
    -- accelerating
    if player.vec.y == 1 then
        player.v = player.v + prefs.player.acc * (1 - (player.v / (prefs.player.v_max + 10)))

        if player.v > prefs.player.v_max then
            player.v = prefs.player.v_max
        end
    -- braking
    elseif player.vec.y == -1 then
        player.v = player.v - prefs.player.brake
        if player.v < 0 then
            player.v = 0
        end
    -- idling
    else
        player.v = player.v - prefs.player.dec
        if player.v < 0 then
            player.v = 0
        end
    end
    -- calculate distance

    game.distance = game.distance + (player.v / 100)

    sfx.car.buf:setPitch(1 + (player.v) * prefs.sfx.car_pitch)

    if game.mode == 5 and player.v <= 0 then
        game.credits()
    end
end

function game.update_pos()
    if player.vec.x == 1 then
        player.pos.x = player.pos.x + prefs.player.v_turn * (player.v / prefs.player.v_max) * (1 / prefs.psp)
        if player.pos.x > prefs.player.h_max * (1 / prefs.psp) then
            player.pos.x = prefs.player.h_max * (1 / prefs.psp)
        end
    elseif player.vec.x == -1 then
        player.pos.x = player.pos.x - prefs.player.v_turn * (player.v / prefs.player.v_max) * (1 / prefs.psp)
        if player.pos.x < -prefs.player.h_max * (1 / prefs.psp) then
            player.pos.x = -prefs.player.h_max * (1 / prefs.psp)
        end
    end
end

function game.update_travel()
    game.trvmode.t = game.trvmode.t + 0.1

    if game.trvmode.t < 15 then
        player.pos.y = player.pos.y - game.trvmode.t / 10
    elseif game.trvmode.t < 25 then
        player.pos.x_travel = player.pos.x_travel - (25-game.trvmode.t) / 10
        player.pos.y = player.pos.y - game.trvmode.t / 100
        player.s = player.s - 0.01
        if player.s < 0.1 then 
            player.s = 0.1
        end
        sfx.car.buf:setVolume(sfx.car.buf:getVolume() - 0.005)
        sfx.car.buf:setPitch(sfx.car.buf:getPitch() + 0.01)
    elseif game.trvmode.t < 45 then
        player.pos.x_travel = player.pos.x_travel + (50-game.trvmode.t) / 10
        player.pos.y = player.pos.y - game.trvmode.t / 80
        game.black = game.black + 0.0078431
        if game.black > 1 then
            game.black = 1
            game.credits()
        end
        sfx.car.buf:setVolume(sfx.car.buf:getVolume() - 0.005)
        sfx.car.buf:setPitch(sfx.car.buf:getPitch() + 0.05)
    end
end

function game.update_travel_prep()
    game.trvmode.t = game.trvmode.t + 0.1
    player.pos.x_prev = player.pos.x

    if game.trvmode.t > 10 then
        game.trvmode.t = 0
        game.mode = 2
    end
end

function game.update_gas()
    for i, obj in ipairs(gas) do
        if obj.dist > game.distance and obj.dist < game.distance + 100 then
            gas[i].draw = true
            game.check_collision_gas(i)
        else
            gas[i].draw = false
        end
        if gas[i] and gas[i].y > 220 then
            gas[i].draw = false
        end
    end
end

function game.check_collision_gas(i)
    if gas[i].y > 200 and gas[i].draw then
        if gas[i].x > -50 and gas[i].x < 50 then
            game.collide(i)
        end
    end
end

function game.collide(i)
    sfx.gas.buf:seek(0)
    sfx.gas.buf:play()

    table.remove(gas, i)

    if game.mode ~=5 then 
        game.flux = game.flux + 1
        if game.flux >= game.flux_max then
            game.travel_init()
        end
    end
end

function game.update_trees()
    for i, obj in ipairs(trees) do
        if obj.dist > game.distance and obj.dist < game.distance + 20 then
            trees[i].draw = true
        else
            trees[i].draw = false
        end
        if trees[i] and trees[i].y > 220 then
            trees[i].draw = false
            table.remove(trees, i)
        end
    end
end

function game.update_timer(dt)
    game.timer = game.timer - dt
    if game.timer <= 0 then
        game.timer = 0
        game.over()
    end
end

function game.update_credits()
    -- working around canvas flashing at game mode change
    if game.black >= 1 then
        game.black = game.black + 0.3

        if game.black >= 2 then
            game.black = 0
        end
    end

    credits.t = credits.t + 1
    credits.x = credits.x - credits.x_v
 
    if credits.x < 0 then
        credits.x = 0
    end

    if credits.t > credits.t_max  then
        credits.t = 0
        credits.x = credits.x_max
        if credits.mode == 1 then
            credits.mode = 2
        else
            credits.mode = 1
        end
    end
end

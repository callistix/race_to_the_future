function game.load_sprites()
    img = {}
    img.car = {}
    img.car.buf = {}
    img.car.buf[1] = love.graphics.newImage("png/delorean.png")
    img.car.buf[2] = love.graphics.newImage("png/delorean_brake.png")
    img.car.buf[3] = love.graphics.newImage("png/delorean_noshadow.png")
    img.car.w, img.car.h = img.car.buf[1]:getDimensions()

    img.tree = {}
    img.tree.buf = love.graphics.newImage("png/tree.png")
    img.tree.w, img.tree.h = img.tree.buf:getDimensions()

    img.tree.quad = {}
    for i = 1, prefs.lod do
        img.tree.quad[i] = love.graphics.newQuad((i - 1) * (img.tree.w / prefs.lod), 0, img.tree.w / prefs.lod, img.tree.h, img.tree.w, img.tree.h)
    end

    img.gas = {}
    img.gas.buf = love.graphics.newImage("png/gas.png")
    img.gas.w, img.gas.h = img.gas.buf:getDimensions()

    level[1].bg = {}
    level[1].bg.buf = love.graphics.newImage("png/background_1.png")
    level[1].bg.w, level[1].bg.h = level[1].bg.buf:getDimensions()
end

function game.load_win()
    love.window.setMode(prefs.can_w * game.scale, prefs.can_h * game.scale)
    love.graphics.setDefaultFilter("nearest", "nearest")

    win.w, win.h = love.graphics.getDimensions()
    win.w = math.floor(win.w / game.scale)
    win.h = math.floor(win.h / game.scale)
    win.split = math.floor(win.h * prefs.split / 100)
end

function game.load_canvas()
    canvas = love.graphics.newCanvas(prefs.can_w, prefs.can_h)
end

function game.load_fonts()
    font = {}
    font.hud = love.graphics.newFont("ttf/SF Square Head, Bold Italic.ttf", prefs.hud.font_size)
    font.credits = love.graphics.newFont("ttf/Block Talk, Regular.ttf", 20)
    font.timer = love.graphics.newFont("ttf/SF Square Head, Bold.ttf", 26)
end

function game.load_sounds()
    sfx = {}
    sfx.car = {}
    sfx.car.buf = love.audio.newSource("wav/engine.wav", "static")
    sfx.car.buf:setLooping(true)
    sfx.car.buf:play()

    sfx.gas = {}
    sfx.gas.buf = love.audio.newSource("wav/gas.wav", "static")

    music = {}
    music.credits = {}
    music.credits.buf = love.audio.newSource("ogg/credits.ogg", "static")
    music.credits.buf:setLooping(true)
    music.credits.buf:setVolume(1)
end

function game.load_trees()
    for i, obj in pairs(trees) do
        trees[i].draw = false
        trees[i].y = 0
    end
end

function game.load_gas()
    for i, obj in pairs(gas) do
        gas[i].draw = false
        gas[i].y = 1
    end
end
